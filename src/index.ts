import { ApolloLink, Operation, NextLink } from '@apollo/client/core';
import { merge } from 'lodash';

import {
  stripServiceDirectives,
  splitOperationByService,
} from './transform/transform';
import { getServiceName, getDirectiveLocation } from './query/query';

export type Service = { name: string; uri: string };

export type DefederationLinkOptions = {
  services: Service[];
};

export class DefederationLink extends ApolloLink {
  constructor(private readonly options: DefederationLinkOptions) {
    super();
  }

  request(
    operation: Operation,
    forward: NextLink,
  ): ReturnType<ApolloLink['request']> {
    try {
      const directiveLocation = getDirectiveLocation(operation.query);

      if (directiveLocation === 'operationDefinition') {
        const handledOperation = handleOperation(operation, this.options);
        return forward(handledOperation);
      }

      // If the directives are defined on the root fields
      // of an operation the operation needs to be split into
      // multiple operations because it is not possible
      // to define multiple uris for one operation
      if (directiveLocation === 'rootField') {
        const operations = splitOperationByService(operation);
        const handledOperations = operations.map((o) =>
          handleOperation(o, this.options),
        );
        const results = handledOperations.map((o) => forward(o));

        // Note: These are two different reduce methods.
        // The first one is the reduce method defined on arrays
        // in which the Observables are chained together.
        // The second one is the reduce method defined on an Observable
        // which merges all incoming values into one singular response

        return results
          .reduce((acc, o) => acc.concat(o))
          .reduce((r, fetchResult) => {
            return merge(fetchResult, r);
          });
      }

      return forward(operation);
    } catch (err) {
      return forward(operation);
    }
  }
}

// Assumes that the operation has already been converted into one
// where either no service directive is present or where the service
// directive is attached to the operation definition.
// Operations which have service direvtives attached to root fields
// are not accepted
function handleOperation(
  operation: Operation,
  options: DefederationLinkOptions,
): Operation {
  // Check if the `OperationDefinition` (which there should only be one of)
  // has an `@service` decorator attached
  const serviceName = getServiceName(operation.query);

  // If not continue with the operation as normal
  if (!serviceName) {
    return operation;
  }

  // Otherwise use the aquired service name to look
  // what the uri of that service is
  const serviceUri = getServiceUri(options.services, serviceName);

  if (!serviceUri) {
    throw new Error(
      `Could not find service with the name "${serviceName}". Did you provide it when creating the DefederationLink?`,
    );
  }

  // Set the uri in the context to use the service's uri
  // so that when the operation gets to the final link
  // the service's uri will be contacted instead of the
  // standard uri
  operation.setContext({ uri: serviceUri });

  // Remove the @service directive from the query of the operation.
  // This is needed because the contacted service is not expected
  // to have a definition for the @service directive, so to avoid
  // type errors it is removed, since it does not have any use beyond
  // this link.
  const document = operation.query;
  const strippedDocument = stripServiceDirectives(document);
  operation.query = strippedDocument;

  return operation;
}

function getServiceUri(services: Service[], name: string): string | undefined {
  return services.find((s) => s.name === name)?.uri;
}

import { gql } from '@apollo/client/core';

import {
  extractUniqueServiceRootFields,
  getDirectiveLocation,
  getOperationDefinition,
  getServiceName,
} from './query';

describe('Query', () => {
  describe('getServiceName', () => {
    test('can extract the service name if the directive is attached to the operation definition', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!)
        @service(name: "product") {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const name = getServiceName(document);
      expect(name).toEqual('product');
    });

    test('returns undefined if no service directive is attached', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const name = getServiceName(document);
      expect(name).toEqual(void 0);
    });

    test('throws error if more than one operation definition is present', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
        query GetDataSourceTable(
          $datasource: String
          $columns: [String!]
          $filters: [DataSourceTableRowFilter!]
        ) {
          dataSourceTable(where: { name: $datasource })
            @service(name: "product") {
            rows(columns: $columns, filters: $filters)
          }
        }
      `;

      const callback = () => {
        getServiceName(document);
      };

      expect(callback).toThrow();
    });

    test('does not extract service name if directive is attached to a field definition', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) @service(name: "product") {
            rowCount
          }
        }
      `;

      const name = getServiceName(document);
      expect(name).toEqual(void 0);
    });
  });

  describe('getDirectiveLocation', () => {
    test('detects if directive is defined on operation definition', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!)
        @service(name: "product") {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const result = getDirectiveLocation(document);

      expect(result).toEqual('operationDefinition');
    });

    test('detects if directive is defined on root field', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) @service(name: "product") {
            rowCount
          }
        }
      `;

      const result = getDirectiveLocation(document);

      expect(result).toEqual('rootField');
    });

    test('detects if no directive is used', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const result = getDirectiveLocation(document);

      expect(result).toEqual('none');
    });
  });

  describe('getOperationDefinition', () => {
    test('returns operation definition if exactly 1 is contained in the document', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const operationDefinition = getOperationDefinition(document);
      expect(operationDefinition).toBeTruthy();
    });

    test('throws error if there is more than one operation definition defined', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const callback = () => {
        getOperationDefinition(document);
      };
      expect(callback).toThrow();
    });

    test('throws error if there are no operation definitions present in the document', () => {
      const document = gql`
        type Person {
          name: String!
        }
      `;

      const callback = () => {
        getOperationDefinition(document);
      };
      expect(callback).toThrow();
    });
  });

  describe('extractUniqueServiceRootFields', () => {
    test('extract all root fields as untagged if no directive is defined', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const result = extractUniqueServiceRootFields(document);
      expect(result.tagged).toEqual({});
      expect(result.untagged).toHaveLength(2);
    });

    test('extract root fields which have the directive defined', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) @service(name: "product") {
            rowCount
          }
          dataSourceTable(where: { name: $name }) @service(name: "product") {
            rowCount
          }

          dataSourceTable(where: { name: $name }) @service(name: "selector") {
            rowCount
          }
        }
      `;
      const result = extractUniqueServiceRootFields(document);
      expect(result.untagged).toHaveLength(0);
      expect(result.tagged['product']).toHaveLength(2);
      expect(result.tagged['selector']).toHaveLength(1);
    });

    test('can extract root fields that are tagged with the directive and root fields that are not', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) @service(name: "product") {
            rowCount
          }
          dataSourceTable(where: { name: $name }) @service(name: "product") {
            rowCount
          }

          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;
      const result = extractUniqueServiceRootFields(document);
      expect(result.untagged).toHaveLength(1);
      expect(result.tagged['product']).toHaveLength(2);
    });
  });
});

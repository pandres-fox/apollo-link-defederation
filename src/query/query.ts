import {
  DocumentNode,
  OperationDefinitionNode,
  FieldNode,
  visit,
} from 'graphql';

import { isServiceDirective } from '../checks/checks';

// Assumes that the document only has an @service directive
// on an operation definition
export function getServiceName(document: DocumentNode): string | undefined {
  const operationDefinition = getOperationDefinition(document);

  // Find if there is an @service directive attached to the OperationDefinition
  // and if so return the name of the service in that directive
  for (const directive of operationDefinition.directives) {
    if (isServiceDirective(directive)) {
      // We only care that the service name is the first argument to
      // the directive and that it is a string. The name of the argument
      // does not matter
      if (directive.arguments[0].value.kind === 'StringValue') {
        return directive.arguments[0].value.value;
      }
    }
  }

  return void 0;
}

export function getServiceNameOnField(field: FieldNode): string | undefined {
  for (const directive of field.directives) {
    if (isServiceDirective(directive)) {
      if (directive.arguments[0].value.kind === 'StringValue') {
        return directive.arguments[0].value.value;
      }
    }
  }

  return void 0;
}

type DirectiveLocation = 'operationDefinition' | 'rootField' | 'none';

// Either the directive is attached to the OperationDefinition
// or to the root fields of the operation
export function getDirectiveLocation(
  document: DocumentNode,
): DirectiveLocation {
  let location: DirectiveLocation = 'none';
  visit(document, {
    OperationDefinition(node) {
      for (const directive of node.directives ?? []) {
        if (isServiceDirective(directive)) {
          location = 'operationDefinition';
          break;
        }
      }
    },
    Field(node) {
      for (const directive of node.directives ?? []) {
        if (isServiceDirective(directive)) {
          location = 'rootField';
          break;
        }
      }
    },
  });

  return location;
}

export type UniqueRootFields = {
  tagged: { [serviceName: string]: FieldNode[] };
  untagged: FieldNode[];
};

export function extractUniqueServiceRootFields(
  document: DocumentNode,
): UniqueRootFields {
  const uniqueRootFields: UniqueRootFields = {
    tagged: {},
    untagged: [],
  };
  const rootFields = extractRootFields(document);

  for (const field of rootFields) {
    const serviceName = getServiceNameOnField(field);

    if (!serviceName) {
      uniqueRootFields.untagged.push(field);
      continue;
    }

    if (!uniqueRootFields.tagged[serviceName]) {
      uniqueRootFields.tagged[serviceName] = [];
    }

    uniqueRootFields.tagged[serviceName].push(field);
  }

  return uniqueRootFields;
}

function extractRootFields(document: DocumentNode): FieldNode[] {
  const operationDefinition = getOperationDefinition(document);
  const rootFields = operationDefinition.selectionSet.selections.filter(
    (s): s is FieldNode => s.kind === 'Field',
  );
  return rootFields;
}

// A valid document should only every include one operation defintion
export function getOperationDefinition(
  document: DocumentNode,
): OperationDefinitionNode {
  const operationDefinitions = document.definitions.filter(
    (d): d is OperationDefinitionNode => d.kind === 'OperationDefinition',
  );

  if (operationDefinitions.length !== 1) {
    throw new Error(
      `The document should contain 1 OperationDefinition. Instead ${operationDefinitions.length} OperationDefinitions were found`,
    );
  }

  return operationDefinitions[0];
}

import { gql } from '@apollo/client/core';
import {
  FieldNode,
  FragmentDefinitionNode,
  OperationDefinitionNode,
} from 'graphql';

import { isFragmentInUse } from './checks';

describe('Checks', () => {
  describe('isFragmentInUse', () => {
    test('detects if a fragment is in use', () => {
      const document = gql`
        query Fetch($selectorId: ID!) @service(name: "selector") {
          selector(where: { id: $selectorId }) {
            ...selectorName
          }
        }

        fragment selectorName on Selector {
          metaName
        }
      `;

      const fields = (document.definitions[0] as OperationDefinitionNode)
        .selectionSet.selections as FieldNode[];
      const fragment = document.definitions[1] as FragmentDefinitionNode;

      const result = isFragmentInUse(fragment, fields);
      expect(result).toBe(true);
    });

    test('detects if a fragment is not in use', () => {
      const document = gql`
        query Fetch($selectorId: ID!) @service(name: "selector") {
          selector(where: { id: $selectorId }) {
            metaName
          }
        }

        fragment selectorName on Selector {
          metaName
        }
      `;

      const fields = (document.definitions[0] as OperationDefinitionNode)
        .selectionSet.selections as FieldNode[];
      const fragment = document.definitions[1] as FragmentDefinitionNode;

      const result = isFragmentInUse(fragment, fields);
      expect(result).toBe(false);
    });
  });
});

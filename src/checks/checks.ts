import {
  DirectiveNode,
  FragmentDefinitionNode,
  visit,
  FieldNode,
} from 'graphql';

export function isServiceDirective(directive: DirectiveNode): boolean {
  return directive.name.value === 'service';
}

export function isFragmentInUse(
  fragmentDefinition: FragmentDefinitionNode,
  fields: FieldNode[],
): boolean {
  const fragmentName = fragmentDefinition.name.value;

  let inUse = false;
  for (const field of fields) {
    visit(field, {
      FragmentSpread(node) {
        if (node.name.value === fragmentName) {
          inUse = true;
        }
      },
    });
  }

  return inUse;
}

import { Operation } from '@apollo/client/core';
import { createOperation } from '@apollo/client/link/utils';
import {
  DirectiveNode,
  DocumentNode,
  OperationDefinitionNode,
  FieldNode,
  VariableDefinitionNode,
  visit,
  ASTNode,
} from 'graphql';

import { isFragmentInUse, isServiceDirective } from '../checks/checks';
import {
  getOperationDefinition,
  extractUniqueServiceRootFields,
} from '../query/query';

type HasDirectives = { directives: ReadonlyArray<DirectiveNode> };

export function splitOperationByService(operation: Operation): Operation[] {
  const documents = splitDocumentByService(operation.query);
  const context = operation.getContext();
  const operations = documents.map((document) => {
    return createOperation({ ...context }, { ...operation, query: document });
  });

  return operations;
}

export function stripServiceDirectives<N extends ASTNode>(ast: N): N {
  const doc = visit(ast, {
    leave(node) {
      if (node.hasOwnProperty('directives')) {
        const n = node as HasDirectives;
        const directives = n.directives.filter((d) => !isServiceDirective(d));
        return {
          ...n,
          directives,
        };
      }
    },
  });

  return doc;
}

export function splitDocumentByService(document: DocumentNode): DocumentNode[] {
  const operationDefinition: OperationDefinitionNode =
    getOperationDefinition(document);
  const uniqueRootFieldsByService = extractUniqueServiceRootFields(document);

  const documents = [];

  for (const serviceName of Object.keys(uniqueRootFieldsByService.tagged)) {
    documents.push(
      createDocumentWithNewRootFields(
        document,
        uniqueRootFieldsByService.tagged[serviceName],
        operationDefinition.variableDefinitions ?? [],
        serviceName,
      ),
    );
  }

  // If there are any root fields which do not have a service directives
  // attached also create a new document containing those fields
  if (uniqueRootFieldsByService.untagged.length > 0) {
    documents.push(
      createDocumentWithNewRootFields(
        document,
        uniqueRootFieldsByService.untagged,
        operationDefinition.variableDefinitions ?? [],
      ),
    );
  }

  return documents;
}

function createDocumentWithNewRootFields(
  baseDocument: DocumentNode,
  rootFields: FieldNode[],
  variableDefinitions: Readonly<VariableDefinitionNode[]>,
  serviceName?: string,
): DocumentNode {
  const operationDefinition = getOperationDefinition(baseDocument);
  const otherDefinitions = baseDocument.definitions.filter((d) => {
    if (d.kind === 'OperationDefinition') {
      return false;
    }

    if (d.kind === 'FragmentDefinition') {
      return isFragmentInUse(d, rootFields);
    }

    return true;
  });
  const variables = variableDefinitions.filter((v) => {
    return rootFields.some((f) => isVariableInUse(v, f));
  });
  return {
    ...baseDocument,
    definitions: [
      {
        ...operationDefinition,
        directives: [
          ...operationDefinition.directives,
          ...(!!serviceName ? [createServiceDirectiveNode(serviceName)] : []),
        ],
        variableDefinitions: variables,
        selectionSet: {
          ...operationDefinition.selectionSet,
          selections: rootFields.map((f) => stripServiceDirectives(f)),
        },
      },
      ...otherDefinitions,
    ],
  };
}

function createServiceDirectiveNode(serviceName: string): DirectiveNode {
  return {
    kind: 'Directive',
    name: {
      kind: 'Name',
      value: 'service',
    },
    arguments: [
      {
        kind: 'Argument',
        name: { kind: 'Name', value: 'name' },
        value: { block: false, kind: 'StringValue', value: serviceName },
      },
    ],
  };
}

function isVariableInUse(
  variableDefinition: VariableDefinitionNode,
  field: FieldNode,
): boolean {
  let inUse = false;

  visit(field, {
    Variable(node) {
      if (node.name.value === variableDefinition.variable.name.value)
        inUse = true;
    },
  });

  return inUse;
}

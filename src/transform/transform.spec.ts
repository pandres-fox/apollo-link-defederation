import { gql, DocumentNode } from '@apollo/client/core';

import { splitDocumentByService, stripServiceDirectives } from './transform';

describe('Transform', () => {
  describe('stripServiceDirectives', () => {
    test('strips out service directive as part of a query definition', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!)
        @service(name: "product") {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const expectedDocument = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const result = stripServiceDirectives(document);

      expect(result).toEqual(expectedDocument);
    });

    test('strips out service directive as part of field definition', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) @service(name: "product") {
            rowCount
          }
        }
      `;

      const expectedDocument = gql`
        query DataSourceTableRowCount($name: String!) {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const result = stripServiceDirectives(document);

      expect(result).toEqual(expectedDocument);
    });

    test('does not strip out other directives', () => {
      const document = gql`
        query DataSourceTableRowCount($name: String!)
        @service(name: "product")
        @other {
          dataSourceTable(where: { name: $name })
            @service(name: "product")
            @another {
            rowCount
          }
        }
      `;

      const expectedDocument = gql`
        query DataSourceTableRowCount($name: String!) @other {
          dataSourceTable(where: { name: $name }) @another {
            rowCount
          }
        }
      `;

      const result = stripServiceDirectives(document);

      expect(result).toEqual(expectedDocument);
    });
  });

  describe('splitDocumentByService', () => {
    test('when splitting only keeps variables if they are in use', () => {
      const doc = gql`
        query Fetch($name: String!, $selectorId: ID!) {
          dataSourceTable(where: { name: $name }) @service(name: "product") {
            rowCount
          }

          selector(where: { id: $selectorId }) @service(name: "selector") {
            metaName
          }
        }
      `;

      const dataSourceTableDocument = gql`
        query Fetch($name: String!) @service(name: "product") {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const selectorDocument = gql`
        query Fetch($selectorId: ID!) @service(name: "selector") {
          selector(where: { id: $selectorId }) {
            metaName
          }
        }
      `;

      const documents = splitDocumentByService(doc);

      expect(documents).toHaveLength(2);
      expect(documents[0]).toEqual(dataSourceTableDocument);
      expect(documents[1]).toEqual(selectorDocument);
    });

    test('when splitting only keeps fragments when they are in use', () => {
      const doc = gql`
        query Fetch($name: String!, $selectorId: ID!) {
          dataSourceTable(where: { name: $name }) @service(name: "product") {
            rowCount
          }

          selector(where: { id: $selectorId }) @service(name: "selector") {
            ...selectorName
          }
        }

        fragment selectorName on Selector {
          metaName
        }
      `;

      const dataSourceTableDocument = gql`
        query Fetch($name: String!) @service(name: "product") {
          dataSourceTable(where: { name: $name }) {
            rowCount
          }
        }
      `;

      const selectorDocument = gql`
        query Fetch($selectorId: ID!) @service(name: "selector") {
          selector(where: { id: $selectorId }) {
            ...selectorName
          }
        }

        fragment selectorName on Selector {
          metaName
        }
      `;

      const documents = splitDocumentByService(doc);

      expect(documents).toHaveLength(2);
      expect(documents[0]).toEqual(dataSourceTableDocument);
      expect(documents[1]).toEqual(selectorDocument);
    });
  });
});

jest.mock('@apollo/client/core', () => {
  const { gql } = jest.requireActual('@apollo/client/core');
  return {
    gql: (value: TemplateStringsArray): DocumentNode => {
      const document = gql(value);
      return {
        ...document,
        loc: void 0,
      };
    },
  };
});
